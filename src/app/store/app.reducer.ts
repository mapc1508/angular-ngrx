// file that merges all reducer functions from different modules
// sets the global application state
import * as fromShoppingList from '../shopping-list/store/shopping-list.reducer';
import * as fromAuth from '../auth/store/auth.reducer';
import { ActionReducerMap } from '@ngrx/store';

export interface AppState {
    // substates - parts of the global state object
    shoppingList: fromShoppingList.State;
    auth: fromAuth.State;
};

// one state that is constitued of substates
// reducerMap is type that is supplied to the app module
// don't need to supply it as dynamic parameter anymore
/*
    Example:
    StoreModule.forRoot({
        shoppingList: shoppingListReducer, 
        auth: authReducer
    })
*/
export const appReducer: ActionReducerMap<AppState> = {
    shoppingList: fromShoppingList.shoppingListReducer,
    auth: fromAuth.authReducer
};
